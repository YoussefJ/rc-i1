import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  // create a reactive model
  userForm: FormGroup;
  errorMessage = ''
  loginMessage = ''
  constructor(private _snackBar: MatSnackBar) {

    this.userForm = new FormGroup({
      username: new FormControl('', [Validators.required]),
      password: new FormControl('', [Validators.required]),
    });

  }

  ngOnInit(): void {
  }

  login(): void {
    this.getError()
    this.loginMessage = "Login Successful";
    this._snackBar.open(this.loginMessage);
  }

  getError() {
    if (this.userForm.controls['username'].touched && !this.userForm.controls['username'].valid) {
      this.errorMessage =  'You must enter a value';
    }
  }

}
