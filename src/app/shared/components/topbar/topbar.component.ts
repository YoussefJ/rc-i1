import { Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { SidenavComponent } from '../sidenav/sidenav.component';

@Component({
  selector: 'app-topbar',
  templateUrl: './topbar.component.html',
  styleUrls: ['./topbar.component.scss']
})
export class TopbarComponent implements OnInit {
  @Output() menuToggle = new EventEmitter();

  constructor() {}

  ngOnInit(): void {
  }

  toggleMenu() {
    this.menuToggle.emit();
  }

}
